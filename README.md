#Development

Make sure you have [Node](http://nodejs.org/), [Grunt](http://gruntjs.com/) and [Bower](http://bower.io/) installed.

To install dependencies, use `npm install && bower install`.

To build all necessary files, use `grunt build`.

To update GPT, use `grunt build_gpt`.

To build JavaScript (includes jshint tests), use `grunt build_js`.

To build CSS, use `grunt build_css`.

To watch for changes, use `grunt watch`.

To start a server at http://localhost:5000 and watch for changes with livereload enabled, use `grunt server`.

To run Qunit tests, use `grunt test`.

To download live pages and replace all ad script references to local resources for testing, use `grunt testpages`.

Using `grunt`, will do all of the above.

For more information and a list of commands, use `grunt -h`.



#Implementation


##Traditional Implementation (inline placeAd2)



###Ad Script

If you are using placeAd2 to render ads, simply replace the following existing ad scripts in the `<head></head>` of the page, below where commercialNode is defined:

    <script src="http://js.washingtonpost.com/wp-srv/ad/generic_ad.js"></script>
    <script src="http://js.washingtonpost.com/wp-srv/ad/slate.js"></script>

With:

    <script src="[YOUR PATH]/js/min/loader.min.js"></script>

It is also possible to load this script at the bottom of the page and then use data attributes attached to elements to render ads (see "Preferred Implementation" below).


###Ad Spot Coding with placeAd2:

Each placeAd2 call can remain unchanged on the page for backwards compatibility:

    <script>
      placeAd2(commercialNode, [pos value]:String, [delivery type]:String|false, [on the fly keyvalues]:String);
    </script>


Although, the new implementation of placeAd2 is preferred:

    <script>
      placeAd2({
        where: commercialNode (OPTIONAL),
        what: [pos value]:String,
        delivery: [delivery type]:String (OPTIONAL - use "vi" for ads to be rendered on view),
        onTheFly: [on the fly keyvalues]:String (OPTIONAL)
      });
    </script>



##Preferred Implementation (full asyc, data attributes)

Inline placeAd2 calls are no longer needed using this method (but, placeAd2 can be called **after** the ad script has loaded if needed for ad spot refreshes, etc).

Most basic ad spot syntax, placed in the position the ad is supposed to render, **before** the ad script is loaded:

    <div id="slug_[POS]" data-ad-type="[POS]"></div>

Where `[POS]` should be the pos value of the ad (leaderboard, bigbox, pushdown, etc.).

####Additional ad spots of the same type on a given page

    <div id="slug_[POS]_2" data-ad-type="[POS]|2"></div>
    <div id="slug_[POS]_3" data-ad-type="[POS]|3"></div>
    <div id="slug_[POS]_4" data-ad-type="[POS]|4"></div>

Etc...



####Additional attributes

The following attributes can be added to the `#slug_[POS]` element (ad spot) to further customise the ad call if needed (just like the arguments in placeAd2):

commercialNode override example:

    data-ad-where="politics"

On the fly keyvalues example:

    data-ad-on-the-fly="newKeyvalue=1"

This example will deliver the ad as a "viewable impression" (only once it is in view).

    data-ad-delivery="vi"


#Config (js/modules/slate/config.js and js/modules/slate_mobile/config.js)

##flights

You will need to manually open and close any spots that aren't open by default by scheduling them in config.js.
Follow the format in config.js. Any `what`, `where`, `when` and `test` tests for each flight must pass in order for the ad to render.


###what [required]

The pos value of the ad/ads passed in as an array (eg: `['leaderboard', 'pushdown']`). Corresponds directly to `adTypes`
object in this same file. You can disable a particular ad spot by prepending a `!`. For example, if you the leaderboard
was enabled by default, but I wanted to disable a leaderboard and enable a pushdown, you would use `['!leaderboard', 'pushdown']`.


###where [optional]

The commercialNode to schedule this spot for. If this omitted, the ad spot will be enabled everywhere. For example, to schedule
an ad flight on the homepage only, the value would be ['homepage']. Multiple commercialNodes can be passed in to the array.


###when [optional]

Used for scheduling ad flights in the format of `[START DATE]/[END DATE]`. If this is omitted, there will be no time constraints on the ad flight. `[START DATE]` and `[END DATE]` should be entered in the format of yyymmddhhmm. For example, January 5th 2014 at 11pm would translate to 201401052300 or December 25th 2015 at 1am would translate to 201512250100. Note that hours/minutes are in 24 hour time format. Scheduling an ad flight between these to dates would look like `['201401052300/201512250100']`. Multiple date ranges can be passed in to the array.

You can also be less specific. For example, to schedule all of 2014, simply use `['2014']`. To schedule all of January in 2014, use `['201401']`. To schedule the 12th of January in 2014, use `['20140112']`. You can still use a `/` for a range of dates using this format. For exmaple, to schedule an ad flight from March 10th 2014 to April 10th 2014, you could use `['20140310/20140410']`.


###hardcode [optional]

This prevents an ad call to dfp and hardcodes code to the ad spot. This accepts either string of HTML or a function that returns either a string of HTML or a DOM Element to append to the ad spot.


###test [optional]

This can be used for more complex conditional checks (other than what, where and when) for scheduling ad spots. Accepts a function and this function should return `true` if the tests pass, or `false` if they fail.


###Example 1

To open the pushdown and disable the rightflex on the homepage on July 20th 2014 and July 22nd 2014:

    {
      what: ['pushdown', '!rightflex'],
      where: ['homepage'],
      when: ['20140720', '20140722']
    }

###Example 2

Display a hardcoded image (not from dfp) in the rightflex on `news` and `sports` commercialNode pages for all of 2014 if `somevar === 20`:

    {
      what: ['rightflex'],
      where: ['news', 'sports'],
      when: ['2014'],
      hardcode: function(){
        var img = document.createElement('img');
        img.src = 'someimage.png';
        return img;
      },
      test: function(){
        return somevar === 20;
      }
    }

###Example 3

To open the rightflex everywhere all the time:

    {
      what: ['rightflex']
    }

##NOTES:

+  You can debug the page by using `?debugAdCode`, the bookmarklet, or just press `ctrl+F9`.
